<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArticleAdditionalData extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'article_id', 'price', 'priceValue', 'size', 'composition', 'manufacturer'
    ];

    public function article()
    {
        return $this->belongsTo('App\Models\Article');
    }
}
