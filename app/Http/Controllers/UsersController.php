<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class UsersController extends Controller
{
    use AuthenticatesUsers;

    private $rules = ['email' => 'required|string|email|max:32|unique:users,email,',
        'password' => 'required|string|min:6|max:40',
        'confirmPass' => 'required|string|min:6|max:40|same:password'];

    private $errors = [
        'email.required' => "Please fill the field.",
        'email.email' => 'Please enter valid email.',
        'email.max' => 'Max lenght of email must be 32.',
        'email.unique' => 'This email is already used.',
        'password.required' => "Please fill the field.",
        'password.confirmed' => 'Please confirm the password.',
        'password.min' => 'Min length of password is 6.',
        'password.max' => 'Max length of password is 40.',
        'confirmPass.same' => 'Enter matching passwords.'
    ];

    function register(Request $request)
    {
        $validator = Validator::make($request->all(), $this->rules, $this->errors);

        if ($validator->fails()) {
            return redirect('/register')
                ->withErrors($validator)
                ->withInput($request->all());
        }
        try {
            $email = $request->get('email');
            $password = Hash::make($request->get('password'));

            DB::table('users')->insert([
                'email' => $email,
                'password' => $password,
            ]);
            return redirect('/login')->with('message', 'Registered successfully');
        } catch (\Exception $е) {
            return redirect('/register')->with('message', 'There is a problem. Please try again.');
        }
    }

    function login(Request $request)
      {
          $validator = Validator::make($request->all(), [
              'email' => 'required|email',
              'password' => 'required',
          ]);

          if ($validator->fails()) {
              return redirect('/login')
                  ->withErrors($validator)
                  ->withInput($request->all());
          }

        if (Auth::attempt(['email' => $request->get('email'), 'password' => $request->get('password')],
            $request->get('remember'))) {

            return redirect('/welcome')->withCookie(Cookie::make('welcome','my first cookie', 2));
        }
        return redirect('/login')->with('message', 'Wrong email and - or password.');
    }
}
