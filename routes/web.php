<?php

Route::get('/welcome', function () {
    return view('welcome');
});

Route::get('/', function () {
    return view('auth/register');
});

Route::get('/index', 'ArticlesController@index');
Route::post('/store', 'ArticlesController@store')->name('articles.store');
Route::get('/edit', 'ArticlesController@edit')->name('articles.edit');
Route::get('/delete', 'ArticlesController@delete')->name('articles.delete');

Auth::routes();

Route::post('/register', 'UsersController@register')->name('users.register');
Route::post('/login', 'UsersController@login')->name('users.login');

Route::get('/home', 'HomeController@index')->name('home');
