@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            @if (\Session::has('message'))
                <div class="alert alert-info">{{\Session::get('message') }}</div>
            @endif

            <div class="row col-6 text-center">
                <form action="{{route('users.login')}}" method="post">
                    {{csrf_field()}}
                    <fieldset>
                        <div>
                            <div>
                                <label for="email">Email: </label>
                                <input type="email" id="email" name="email" size="32" maxlength="32" required
                                       value="{{ old('email') }}"/>
                                <br/>
                                <small class="text-danger">{{ $errors->first('email') }}</small>
                            </div>
                            <div>
                                <label for="password">Password: </label>
                                <input type="password" id="password" name="password" size="40" maxlength="40" required
                                       value="{{ old('password') }}"/>
                                <br/>
                                <small class="text-danger">{{ $errors->first('password') }}</small>
                            </div>

                            <div>
                                <input class="form-check-input" type="checkbox" name="remember"
                                       id="remember" {{ old('remember') ? 'checked' : '' }}>
                                <label class="form-check-label" for="remember">
                                    {{ __('Remember Me') }}
                                </label>
                            </div>

                            <input type="submit" class="btn btn-primary" value="Log In"/>
                            @if (Route::has('password.request'))
                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                            @endif
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
@endsection





