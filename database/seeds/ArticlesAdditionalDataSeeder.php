<?php

use Illuminate\Database\Seeder;

class ArticlesAdditionalDataSeeder extends Seeder
{
    private $articlesData = [
        ['article_id' => 1, 'price' => 2, 'priceValue' => 'лв', 'size' => 60, 'composition' => 'пух', 'manufacturer' => 'Китай'],
        ['article_id' => 2, 'price' => 10, 'priceValue' => 'лв', 'size' => 50, 'composition' => 'пух', 'manufacturer' => 'Китай'],
        ['article_id' => 3, 'price' => 3, 'priceValue' => 'лв', 'size' => 60, 'composition' => 'пластмаса', 'manufacturer' => 'България'],
        ['article_id' => 4, 'price' => 8, 'priceValue' => 'лв', 'size' => 60, 'composition' => 'пух', 'manufacturer' => 'България'],
    ];

    public function run()
    {
        foreach ($this->articlesData as $data) {
            DB::table('article_additional_data')->insert([
                'article_id' => $data['article_id'],
                'price' => $data['price'],
                'priceValue' => $data['priceValue'],
                'size' => $data['size'],
                'composition' => $data['composition'],
                'manufacturer' => $data['manufacturer'],
            ]);
        }
    }
}